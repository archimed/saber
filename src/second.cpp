/* 
 * File:   second.cpp
 * Author: archimed
 * 
 * Created on 21 февраля 2015 г., 14:54
 */

#include <cstddef>
#include "second.h"

namespace t2
{

void RemoveDups(char *pStr)
{
    if (!pStr || *pStr == '\0')
        return;

    std::size_t nDuplicateNum = 0;
    char cLastSymb = 0;

    while (*pStr != '\0')
    {
        if (*pStr != cLastSymb)
            *(pStr - nDuplicateNum) = cLastSymb = *pStr;
        else
            nDuplicateNum++;
        pStr++;
    }

    *(pStr - nDuplicateNum) = '\0';
}

} // namespace t2
