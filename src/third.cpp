/* 
 * File:   third.cpp
 * Author: archimed
 * 
 * Created on 21 февраля 2015 г., 15:03
 */

#include <string>
#include <iostream>
#include <vector>
#include <map>

#include "third.h"

namespace t3
{

//==============================================================================

ListNode::ListNode(const char *sData) : m_sData(sData) { }

//==============================================================================

void ListNode::link(ListNode *pPrev, ListNode *pNext, ListNode *pRand)
{
    m_pPrev = pPrev;
    m_pNext = pNext;
    m_pRand = pRand;
}

//==============================================================================

// A class to assign the unique Id to list nodes 
class ListIndex
{
public:
    typedef std::map<ListNode *, std::size_t> tIdMap;

    // Input: 
    //        nullptr   : do not assign unique Id
    //        ptr       : pointer to node
    // Output:
    //        0           : if ptr is nullptr
    //        1-n         : otherwise
    std::size_t getId(ListNode *pNode)
    {
        if (!pNode)
            return 0;

        tIdMap::iterator i = m_IdMap.find(pNode);
        if (i != m_IdMap.end())
            return i->second;
        else
        {
            std::size_t nId = m_IdMap.size() + 1;
            m_IdMap.emplace(pNode, nId);
            return nId;
        }
    }

private:
    tIdMap m_IdMap;
};

//==============================================================================

// A class to create list nodes by the unique Id
class NodeIndex
{
public:
    typedef std::vector<ListNode *> tIdMap;

    NodeIndex(std::size_t nSize) :
    m_IdMap(nSize, nullptr) { }

    // Input: 
    //        0   : do not create node, nullptr
    //        1-n : return the node with such id (allocate, if id is met the first time)
    // Output:
    //        nullptr     : Id is 0 or wrong
    //        ptr to node : otherwise
    ListNode * getNode(std::size_t nId)
    {
        if (nId == 0)
            return nullptr;

        if (nId > m_IdMap.size())
            return nullptr;

        if (m_IdMap[nId - 1])
            return m_IdMap[nId - 1];
        else
            return m_IdMap[nId - 1] = new ListNode;
    }

private:
    tIdMap m_IdMap;
};

//==============================================================================

List::List(ListNode *pHead, ListNode *pTail)
{
    if (!pHead || !pTail)
    {
        // TODO: handle, e.g. throw exception
        return;
    }

    ListNode *i = pHead;
    std::size_t n = 1;

    while (i->m_pNext)
    {
        n++;
        i = i->m_pNext;
    }

    if (i != pTail)
    {
        // TODO: handle, e.g. throw exception
        return;
    }

    m_pHead = pHead;
    m_pTail = pTail;
    m_nCount = n;
}

//==============================================================================

const ListNode * List::Head() const
{
    return m_pHead;
}

//==============================================================================

const ListNode * List::Tail() const
{
    return m_pTail;
}

//==============================================================================

std::size_t List::Size() const
{
    return m_nCount;
}

//==============================================================================

void List::Serialize(std::ostream &stream)
{
    ListIndex IndexDB;
    ListNode *i = m_pHead;

    stream << m_nCount << std::endl;
    stream << IndexDB.getId(m_pHead) << std::endl;
    stream << IndexDB.getId(m_pTail) << std::endl;

    while (i)
    {
        stream << IndexDB.getId(i) << std::endl;
        stream << IndexDB.getId(i->m_pNext) << std::endl;
        stream << IndexDB.getId(i->m_pPrev) << std::endl;
        stream << IndexDB.getId(i->m_pRand) << std::endl;
        stream << i->m_sData << std::endl;
        i = i->m_pNext;
    }
}

//==============================================================================

void List::Deserialize(std::istream &stream)
{
    // it is better to use RAII object to restore mask in destructor
    std::ios_base::iostate OldState = stream.exceptions(); // save stream exception flags
    stream.exceptions(std::ios_base::eofbit | std::ios_base::badbit | std::ios_base::failbit);

    try
    {
        std::size_t nHeadId = 0, nTailId = 0;
        stream >> m_nCount;

        NodeIndex IndexDB(m_nCount);

        stream >> nHeadId;
        stream >> nTailId;

        m_pHead = IndexDB.getNode(nHeadId);
        m_pTail = IndexDB.getNode(nTailId);

        for (std::size_t i = 0; i < m_nCount; i++)
        {
            std::size_t nId = 0;
            std::size_t nNextId = 0;
            std::size_t nPrevId = 0;
            std::size_t nRandId = 0;
            std::string sData;

            stream >> nId;
            stream >> nNextId;
            stream >> nPrevId;
            stream >> nRandId;
            stream.ignore(); // skip the previous linewrap
            std::getline(stream, sData); // FIXME: deserialization fails on linewrap in string. Change it, if necessary

            ListNode *pNode = IndexDB.getNode(nId);
            if (!pNode) // TODO: deserialization failed, do something (e.g. throw an exception)
                throw std::exception();
            pNode->m_pNext = IndexDB.getNode(nNextId);
            pNode->m_pPrev = IndexDB.getNode(nPrevId);
            pNode->m_pRand = IndexDB.getNode(nRandId);
            pNode->m_sData = sData;
        }
    }
    catch (std::ios_base::failure &e)
    {
        // TODO: deserialization failed, do something (e.g. throw an exception)
    }
    catch (std::exception &e) // we should restore stream's flags, catch all possible exceptions
    {
        // TODO: deserialization failed, do something (e.g. throw an exception)
    }

    // Maybe, it is necessary to check a list for outstanding nodes

    stream.exceptions(OldState);
}

//==============================================================================

} // namespace t3

