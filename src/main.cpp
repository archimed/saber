/* 
 * File:   main.cpp
 * Author: archimed
 *
 * Created on 21 февраля 2015 г., 14:38
 */

#include <iostream>
#include <fstream>
#include <string>

#include "first.h"
#include "second.h"
#include "third.h"

int main()
{
    {
        // The first task
        t1::bitprint(std::cout, 0xFF) << std::endl;
    }

    {
        // The second task
        char pStr[] = "AAA BBB AAA";
        t2::RemoveDups(pStr);
        std::cout << pStr << std::endl;
    }


    {
        // The third task
        
        // List:
        //
        //                              0
        //                              ^
        //                              |v--------|
        //   N1 <---> N2 <---> N3 <---> N4 <---> N5
        //    ^-------|---------^                 ^
        //            |---------------------------|


        {
            t3::ListNode N1("Node 1");
            t3::ListNode N2("Node2");
            t3::ListNode N3("Node 3");
            t3::ListNode N4;
            t3::ListNode N5("Node \t 4");

            N1.link(nullptr, &N2, &N3);
            N2.link(&N1, &N3, &N5);
            N3.link(&N2, &N4, &N1);
            N4.link(&N3, &N5);
            N5.link(&N4, nullptr, &N4);

            t3::List List(&N1, &N5);

            std::ofstream out("data.txt");
            if (!out)
            {
                std::cerr << "Unable open file to write" << std::endl;
                return -1;
            }

            List.Serialize(out);
        }

        t3::List List;

        {
            std::ifstream in("data.txt");
            if (!in)
            {
                std::cerr << "Unable open file to write" << std::endl;
                return -1;
            }

            List.Deserialize(in);
        }

        auto i = List.Head();
        std::cout << "List: " << std::endl;
        std::cout << "=================================================" << std::endl;
        while (i)
        {
            std::cout << "Node: " << i << std::endl;
            std::cout << "Prev: " << i->m_pPrev << std::endl;
            std::cout << "Next: " << i->m_pNext << std::endl;
            std::cout << "Rand: " << i->m_pRand << std::endl;
            std::cout << "Data: " << i->m_sData << std::endl;
            std::cout << std::endl;
            i = i->m_pNext;
        }
    }


    return 0;
}

