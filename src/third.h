/* 
 * File:   third.h
 * Author: archimed
 *
 * Created on 21 февраля 2015 г., 15:03
 */

#pragma once

#include <ios>

namespace t3
{

struct ListNode
{
    ListNode() = default;
    explicit ListNode(const char *sData);
    void link(ListNode *pPrev, ListNode *pNext, ListNode *pRand = nullptr);

    ListNode *m_pPrev = nullptr;
    ListNode *m_pNext = nullptr;
    ListNode *m_pRand = nullptr;
    std::string m_sData;
};

class List
{
public:
    List() = default;
    List(ListNode *pHead, ListNode *pTail);

public:
    void Serialize(std::ostream &stream);
    void Deserialize(std::istream &stream);

    const ListNode * Head() const;
    const ListNode * Tail() const;
    std::size_t Size() const;

private:
    ListNode *m_pHead = nullptr;
    ListNode *m_pTail = nullptr;
    std::size_t m_nCount = 0;
};

} // namespace t3

