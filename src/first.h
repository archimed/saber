/* 
 * File:   first.h
 * Author: archimed
 *
 * Created on 21 февраля 2015 г., 14:38
 */

#pragma once

#include <ios>

namespace t1
{

#define GET_BIT(NUMBER, BIT) (((NUMBER) >> (BIT)) & 0x01)


// works only for integer types

template<class Type>
std::ostream & bitprint(std::ostream &stream, Type number)
{
    static const char C[] = {'0', '1'};
    std::size_t nBitSize = sizeof (number) * 8;
    do
    {
        stream << C[GET_BIT(number, --nBitSize)]; // "stream << char" is faster a bit, than "stream << number"
    } while (nBitSize > 0);
    return stream;
}

} // namespace t1